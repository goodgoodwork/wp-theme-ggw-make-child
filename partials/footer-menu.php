<?php
/**
 * @package Make
 */
if ( has_nav_menu('footer') ):
?>
<nav id="footer-navigation" class="footer-navigation" role="navigation">
  <a class="footer-nav-image" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/public/img/twin-pines.png" alt="Twin Pine Logo" width="70" height="70"></a>
  <?php
  wp_nav_menu( array(
    'theme_location' => 'footer',
    'fallback_cb'    => false,
  ) );
  ?>
</nav>
<?php endif; ?>
