<?php
// Add call to action setting to sections

function make_child_add_settings( $settings, $section_type ) {
    // We're interested only in sections, not section items.
    if ( ! in_array( $section_type, array(
        'text', 'banner', 'gallery', 'panels', 'postlist', 'productgrid', 'downloads'
        ) ) ) {
        return $settings;
    }
    // We're inserting the control right before the "Background" tab.
    $index = array_search( 'divider-background', wp_list_pluck( $settings, 'name' ) );
    $settings[$index - 25] = array(
        'type'    => 'checkbox',
        'label'   => __( 'Call to action', 'make' ),
        'name'    => 'call-to-action',
        'default' => ttfmake_get_section_default( 'call-to-action', $section_type ),
    );
    $settings[$index - 26] = array(
        'type'    => 'checkbox',
        'label'   => __( 'Call out', 'make' ),
        'name'    => 'call-out',
        'default' => ttfmake_get_section_default( 'call-out', $section_type ),
    );
    return $settings;
}
/**
 * Specifies a default value for the custom setting.
 */
function make_child_setting_defaults( $defaults ) {
    // We're interested only in sections, not section items.
    foreach ( $defaults as $section_id => $section_defaults ) {
        if ( ! in_array( $section_defaults['section-type'], array(
            'text', 'banner', 'gallery', 'panels', 'postlist', 'productgrid', 'downloads'
            ) ) ) {
            continue;
        }
        $defaults[$section_id]['call-to-action'] = 0;
        $defaults[$section_id]['call-out'] = 0;
    }
    return $defaults;
}
/**
 * Sanitize the custom setting value when saving the page.
 */
function make_child_call_to_action_save_data( $clean_data, $original_data ) {
    if ( isset( $original_data['call-to-action'] ) && 1 == $original_data['call-to-action'] ) {
        $clean_data['call-to-action'] = 1;
    } else {
        $clean_data['call-to-action'] = 0;
    }
    return $clean_data;
}
function make_child_call_out_save_data( $clean_data, $original_data ) {
    if ( isset( $original_data['call-out'] ) && 1 == $original_data['call-out'] ) {
        $clean_data['call-out'] = 1;
    } else {
        $clean_data['call-out'] = 0;
    }
    return $clean_data;
}
/**
 * Filter the sections html class.
 */
function make_child_call_to_action_html_class( $classes, $section_data ) {
    if ( 1 === $section_data['call-to-action'] ) {
        $classes .= ' cta';
    } elseif ( 1 === $section_data['call-out'] ) {
        $classes .= ' call-out';
    }
    return $classes;
}
add_filter( 'make_section_settings', 'make_child_add_settings', 60, 2 );
add_filter( 'make_sections_defaults', 'make_child_setting_defaults' );
add_filter( 'make_prepare_data_section', 'make_child_call_to_action_save_data', 30, 2 );
add_filter( 'make_prepare_data_section', 'make_child_call_out_save_data', 30, 2 );
add_filter( 'make_section_html_class', 'make_child_call_to_action_html_class', 20, 2 );
