<?php

// Register Footer Menu
function register_footer_menu() {
  register_nav_menu('footer',__( 'Footer Menu' ));
}
add_action( 'after_setup_theme', 'register_footer_menu' );
